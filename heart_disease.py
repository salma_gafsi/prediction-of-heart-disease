# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 10:20:07 2020

@author: Salma
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns 
import matplotlib


heart = pd.read_csv(r'C:\Users\Salma\Desktop\Heart_Disease\heart.csv)
print(heart.head())
print(heart.info())
print(heart.describe())
print(heart.isnull().sum())
print(heart.corr())
heart=heart.drop('fbs',axis=1)

matplotlib.rcParams['figure.figsize'] =[10,7]
matplotlib.rcParams.update({'font.size': 12})
matplotlib.rcParams['font.family'] = 'sans-serif'
heart.hist(figsize=(15,15),edgecolor='black')


sns.countplot(x='target',data=heart)
plt.title('target = 1 or 0')

sns.countplot(x='sex',hue='target',data=heart)
plt.title('1 = positif; 0 = negatif')
plt.xlabel('sex => 1 = male; 0 = female')

sns.countplot(x='age',data=heart)

from sklearn.model_selection import train_test_split 
from sklearn import metrics
from sklearn.metrics import accuracy_score
x=heart.drop('target',axis=1)
y=heart.target
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.20)
print(x_train.shape,y_train.shape,x_test.shape,y_test.shape)

###########Linear regression #########

from sklearn import linear_model
#Train the model
model = linear_model.LinearRegression()
#Fit the model
model.fit(x_train, y_train)
y_pred = model.predict(x_test)
#Score/Accuracy
print("Accuracy --> ", model.score(x_test, y_test)*100)


############ Logistic regression #########
from sklearn.linear_model import LogisticRegression
model = LogisticRegression()
#Fit the model
model.fit(x_train, y_train)
y_pred = model.predict(x_test)
#Score/Accuracy
print("Accuracy --> ", model.score(x_test, y_test)*100)


############ KNN #########
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors=20)
knn.fit(x_train, y_train)

y_pred = knn.predict(x_test)
#Score/Accuracy
print("Accuracy --> ", knn.score(x_test, y_test)*100)


############ SVM #########
from sklearn import svm

#Create a svm Classifier
clf = svm.SVC(kernel='linear') # Linear Kernel

#Train the model using the training sets
clf.fit(x_train, y_train)

#Predict the response for test dataset
y_pred = clf.predict(x_test)
#Score/Accuracy
print("Accuracy --> ", clf.score(x_test, y_test)*100)
